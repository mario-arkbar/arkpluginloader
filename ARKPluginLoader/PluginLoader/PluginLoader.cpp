/*

ARK Server Plugin Loader
Loads Server Plugins from the .\plugins\ folder

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#include "PluginLoader.h"
#include <map>
#include <iostream>
#include <cassert>

static HMODULE caller;
static unsigned int pluginsCount = 0;

PluginList CPluginLoader::pluginMap;

BOOL CPluginLoader::LoadPlugin(string fileName)
{
	HMODULE handle;
	string libraryName;
	CHAR message[MAX_PATH] = { 0 };

	libraryName = pluginPath;
	libraryName.append(fileName);

	handle = LoadLibrary(libraryName.c_str());
	if (!handle)
		return false;

	string niceName = fileName.substr(0, fileName.length() - 4);
	pluginMap[niceName] = handle;

	return true;
}

int CPluginLoader::LoadPlugins(HMODULE hModule)
{

	caller = hModule;

	CHAR pluginsPathBuffer[MAX_PATH] = { 0 }, message[MAX_PATH] = { 0 };
	WIN32_FIND_DATA findData;
	HANDLE hFind;
	int iLastError;

	GetModuleFileName(caller, pluginsPathBuffer, MAX_PATH);
	PathRemoveFileSpec(pluginsPathBuffer);
	strcat_s(pluginsPathBuffer, MAX_PATH, "\\plugins\\");
	pluginPath = pluginsPathBuffer;

	strcat_s(pluginsPathBuffer, MAX_PATH, "*.dll");

	console.Message("Loading plugins ...", console.Color::WHITE);
	hFind = FindFirstFile(pluginsPathBuffer, &findData);
	while (hFind != INVALID_HANDLE_VALUE)
	{
		console.Message("----------");
		sprintf_s(message, "Loading: %s", ((string)findData.cFileName).substr(0, ((string)findData.cFileName).length() -4).c_str());
		console.Message(message, console.Color::YELLOW);
		if (!LoadPlugin(findData.cFileName))
		{
			iLastError = GetLastError();
			sprintf_s(message, "Failed to load plugin! (%d)", iLastError);
			console.Message(message, console.Color::RED);
		}
		else
			pluginsCount++;

		if (!FindNextFile(hFind, &findData))
		{
			hFind = INVALID_HANDLE_VALUE;
			continue;
		}
	}
	console.Message("----------");

	return pluginsCount;
}

BOOL CPluginLoader::UnloadPlugin(string name) 
{
	if (pluginMap.find(name) != pluginMap.end()) {
		HMODULE module = pluginMap.at(name);
		FreeLibrary(module);
		char message[MAX_PATH];
		sprintf_s(message, "Unloaded: %s", name);
		console.Message(message);
		return true;
	}
	return false;
}
BOOL CPluginLoader::ReloadPlugin(string name)
{
	if (UnloadPlugin(name)) {
		CHAR pluginsPathBuffer[MAX_PATH] = { 0 }, message[MAX_PATH] = { 0 };
		WIN32_FIND_DATA findData;
		HANDLE hFind;
		int iLastError;
	

		GetModuleFileName(caller, pluginsPathBuffer, MAX_PATH);
		PathRemoveFileSpec(pluginsPathBuffer);
		strcat_s(pluginsPathBuffer, MAX_PATH, "\\plugins\\");
		pluginPath = pluginsPathBuffer;

		strcat_s(pluginsPathBuffer, MAX_PATH, "*.dll");

		hFind = FindFirstFile(pluginsPathBuffer, &findData);
		while (hFind != INVALID_HANDLE_VALUE)
		{
			std::string file = string(findData.cFileName);
			if (file.substr(0, file.length() -4) == name)
			{
				sprintf_s(message, "Loading: %s", ((string)findData.cFileName).substr(0, ((string)findData.cFileName).length() - 4));
				console.Message(message, console.Color::YELLOW);
				if (!LoadPlugin(findData.cFileName))
				{
					iLastError = GetLastError();
					sprintf_s(message, "Failed to load plugin! (%d)", iLastError);
					console.Message(message, console.Color::RED);
					return false;
				}
				return true;
			}
		}
	}
	return false;
}

CPluginLoader::~CPluginLoader()
{
	// Gracefully free loaded libraries
	for (it_type iterator = pluginMap.begin(); iterator != pluginMap.end(); iterator++) {
		FreeLibrary(iterator->second);
	}
	pluginMap.clear();
}
