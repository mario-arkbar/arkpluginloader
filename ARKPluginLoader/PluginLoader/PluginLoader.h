/*

ARK Server Plugin Loader
Loads Server Plugins from the .\plugins\ folder

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#ifndef _PLUGINLOADER_H
#define _PLUGINLOADER_H

#include <map>
#include <iostream>
#include <cassert>
#include <Windows.h>
#include <vector>
#include <string>
#include <stdlib.h>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

#include "../ConsoleExtension/ConsoleExtension.h"

using namespace std;
typedef std::map<std::string, HMODULE> PluginList;
typedef std::map<std::string, HMODULE>::iterator it_type;

static string pluginPath;

class CPluginLoader
{
	static CConsoleExtension console;

public:
	
	static PluginList pluginMap;
	static int LoadPlugins(HMODULE);
	static BOOL LoadPlugin(string);
	static BOOL UnloadPlugin(string);
	static BOOL ReloadPlugin(string);

	~CPluginLoader();

};


#endif