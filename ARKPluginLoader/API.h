#include <string>
#include <stdlib.h>
#include <Windows.h>

using namespace std;

#define ARKAPI __declspec(dllexport)
#define EXTERN_C     extern "C"

class Console {
public:
	static enum Color {
		DEFAULT = 0x100,
		GREEN = 0x0A,
		RED = 0x0C,
		BLUE = 0x09,
		YELLOW = 0x0E,
		WHITE = 0x0F
	};

};

struct API
{
	virtual void ConsoleMessage(string p, string s);
	virtual void ConsoleMessage(string p, string s, Console::Color c);
	virtual void Release() = 0;
};


class ARKAPI ArkAPI
{
public:
	void ConsoleMessage(string p, string s);
	void ConsoleMessage(string p, string s, Console::Color c);
};

// Handle type. In C++ language the iterface type is used.
typedef API* APIHANDLE;
extern "C" ARKAPI API* APIENTRY GetAPI(VOID);