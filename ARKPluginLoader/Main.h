/*

ARK Server Plugin Loader
Loads Server Plugins from the .\plugins\ folder

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#ifndef _MAIN_H
#define _MAIN_H

#include <Windows.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>

#include "VersionWrapper/VersionWrapper.h"
#include "ConsoleExtension/ConsoleExtension.h"
#include "PluginLoader/PluginLoader.h"
#include "CSocket.h"

using namespace std;

// The exectuable we want to patch into.
#define TARGET_EXE_NAME "ShooterGameServer.exe"

CConsoleExtension console;
CSocket socketClient;
CPluginLoader pluginLoader;

BOOL ProcessAttach(HMODULE);
BOOL ProcessDetach(HMODULE);
BOOL DoInitVersionWrapper();
BOOL CheckIsValidModule();
BOOL DoLoadPlugins(HMODULE);

#endif	// _MAIN_H