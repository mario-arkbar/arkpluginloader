/*

ConsoleExtension
Allows to print stuff into the ARK server console.

Credits for the original idea to "Jeed" @ Steam

*/

#include "../CSocket.h"
#include "ConsoleExtension.h"

static FileLogger logger(LOADER_VERSION, "server.log");

void CConsoleExtension::Message(std::string message, Color color)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hStdOut, &csbi);

	printf("[");
	SetConsoleTextAttribute(hStdOut, 0x0A);
	printf("%s", PROJECT_NAME);
	SetConsoleTextAttribute(hStdOut, 0x07);
	printf("] ");

	// Allow custom color
	if (color != Color::DEFAULT)
	{
		SetConsoleTextAttribute(hStdOut, color);
	}

	printf("%s\n", message.c_str());
	logger << FileLogger::e_logType::LOG_INFO << message.c_str();
	CSocket::Send("log", message.c_str());

	SetConsoleTextAttribute(hStdOut, 0x07);
	FlushConsoleInputBuffer(hStdOut);
}

void CConsoleExtension::Message(string message)
{
	Message(message, Color::DEFAULT);
}

void CConsoleExtension::Message(std::string plugin, std::string message, Color color)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(hStdOut, &csbi);


	printf("[");
	SetConsoleTextAttribute(hStdOut, 0x0A);
	printf("%s", plugin.c_str());
	SetConsoleTextAttribute(hStdOut, 0x07);
	printf("] ");

	// Allow custom color
	if (color != Color::DEFAULT)
	{
		SetConsoleTextAttribute(hStdOut, color);
	}

	printf("%s\n", message.c_str());
	char l[MAX_PATH];
	sprintf_s(l, "[%s] %s", plugin.c_str(), message.c_str());
	logger << FileLogger::e_logType::LOG_INFO << l;
	CSocket::Send("log", "["+ plugin +"] " +message.c_str());

	SetConsoleTextAttribute(hStdOut, 0x07);
	FlushConsoleInputBuffer(hStdOut);
}

void CConsoleExtension::Message(string plugin, string message)
{
	Message(plugin, message, Color::DEFAULT);
}
