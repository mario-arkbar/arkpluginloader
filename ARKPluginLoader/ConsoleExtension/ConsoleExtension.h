/*

ConsoleExtension
Allows to print stuff into the ARK server console.

Credits for the original idea to "Jeed" @ Steam

*/

#ifndef _CONSOLEEXTENSION_H
#define _CONSOLEEXTENSION_H


#include <Windows.h>
#include <stdio.h>
#include <string>
#include "../Logger/Logger.h"
using namespace std;

class CConsoleExtension
{
public:
	static enum Color {
		DEFAULT = 0x100,
		GREEN = 0x0A,
		RED = 0x0C,
		BLUE = 0x09,
		YELLOW = 0x0E,
		WHITE = 0x0F
	};

	static void InitConsole()
	{
		FILE *lpConsoleFile;

		AllocConsole();
		freopen_s(&lpConsoleFile, "CONOUT$", "w", stdout);
	}

	static void Message(string message, Color color);
	static void Message(string message);

	static void Message(string plugin, string message, Color color);
	static void Message(string plugin, string message);
};

#endif	// _CONSOLEEXTENSION_H
