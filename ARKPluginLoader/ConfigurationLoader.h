﻿/*

ARKPluginLoader Configuration Loader


Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabrĺten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#ifndef _PLUGINCONFIGURATIONS_H
#define _PLUGINCONFIGURATIONS_H

#include <Windows.h>
#include <time.h>
#include <string>
#include <sstream>
#include <vector>
#include <Shlwapi.h>
#pragma comment(lib, "shlwapi.lib")

using namespace std;

#define SETTINGS_PATH "ARKPluginLoader.ini"



class CConfigurationLoader
{

public:

	string host;
	int port;

	void LoadConfiguration();

	CConfigurationLoader();
};



static CConfigurationLoader configurations;

#endif	// _CONFIGURATIONS_H