/*

ARK Server Plugin Loader
Loads Server Plugins from the .\plugins\ folder

Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabråten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#include "Main.h"
#include <thread>
#include "ConfigurationLoader.h"

CConfigurationLoader config;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			return ProcessAttach(hModule);
		case DLL_PROCESS_DETACH:
			return ProcessDetach(hModule);
			break;
	}
	return TRUE;
}


HANDLE HndThread;

int Thread()
{
	console.Message("Starting thread");

	return 0;
}

DWORD WINAPI ThreadSocket(LPVOID lpParam)
{
	// Dedicated Socket Thread
	if (config.host != "")
	{
		CSocket::Connect(config.host, config.port);
	}
	else 
	{
		console.Message("Socket is not starting, missing configuration", CConsoleExtension::YELLOW);
	}


	return 1;
}

DWORD WINAPI ThreadLog(LPVOID lpParam)
{
	return 1;
}

BOOL ProcessAttach(HMODULE hModule)
{

	BOOL bResult;
	CHAR message[MAX_PATH] = { 0 };

	bResult = DoInitVersionWrapper();
	if (!bResult)
		return false;

	bResult = CheckIsValidModule();
	if (!bResult)
		return false;

	console.InitConsole();

	console.Message("============================================================");

	sprintf_s(message, "ARK Plugin Loader :: Version %s", LOADER_VERSION);
	console.Message(message, console.Color::WHITE);

	sprintf_s(message, "Copyright(c) 2015 %s", LOADER_AUTHOR);
	console.Message(message, console.Color::WHITE);

	console.Message("");

	// Load Configuration
	config.LoadConfiguration();

	bResult = DoLoadPlugins(hModule);
	if (!bResult)
		return false;

	console.Message("============================================================\n");

	DisableThreadLibraryCalls(hModule);
	DWORD  dwThreadId;
	HANDLE hThreadSocket = CreateThread(
		NULL, 0, ThreadSocket, NULL, 0, &dwThreadId);

	HANDLE hThreadTest = CreateThread(
		NULL, 0, ThreadLog, NULL, 0, &dwThreadId);



	return true;
}

BOOL ProcessDetach(HMODULE hModule)
{
	// Freeing the original version.dll module
	if (hVersion)
		FreeLibrary(hVersion);

	return true;
}

BOOL DoInitVersionWrapper()
{
	// Try initializing the version.dll wrapper (proxy)
	if (!versionWrapperInit())
		return false;

	return true;
}

BOOL CheckIsValidModule()
{
	// Getting the current module name and comparing it to our defined EXE_NAME
	TCHAR buffer[MAX_PATH] = { 0 };
	DWORD bufSize = sizeof(buffer) / sizeof(*buffer);

	GetModuleFileName(NULL, buffer, bufSize);

	string name = PathFindFileName(buffer);
	transform(name.begin(), name.end(), name.begin(), tolower);

	string temp(TARGET_EXE_NAME);
	transform(temp.begin(), temp.end(), temp.begin(), tolower);

	return (name == temp);
}

BOOL DoLoadPlugins(HMODULE hModule)
{
	CHAR message[MAX_PATH] = { 0 };
	unsigned int pluginsCount = pluginLoader.LoadPlugins(hModule);

	sprintf_s(message, "%d plugin%s loaded", pluginsCount, pluginsCount == 1 ? "" : "s");
	console.Message(message);
	return true;
}
