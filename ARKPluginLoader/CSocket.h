/*

Socket Class by Xstasy (Daniel Gothenborg) <daniel@dgw.no>
Allows interaction with server through simple sockets.

*/

#ifndef _CSocket_H
#define _CSocket_H

#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <mutex>
#include <condition_variable>
#include <thread>
#include "ConsoleExtension\ConsoleExtension.h"
#include "PluginLoader\PluginLoader.h"

using namespace std;

class CSocket
{
	static CConsoleExtension console;
public:
	static void Connect(string host, int port);
	static void Init();
	static void Send(string command, string data);
};


#endif