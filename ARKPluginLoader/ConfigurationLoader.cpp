﻿/*

ARKPluginLoader Configuration Loader


Copyright (c) 2015 Team ARK Bar
- Mario Werner
- Daniel Gothenborg
- Alexander Hagabrĺten

Contact mario@108bits.de or daniel@dgw.no for any issues

*/

#include "ConfigurationLoader.h"

CConfigurationLoader::CConfigurationLoader()
{
	host = "";
	port = 0;
}

void CConfigurationLoader::LoadConfiguration()
{
	char buffer[MAX_PATH], settingsPath[MAX_PATH];
	string strResult;

	GetModuleFileName(NULL, settingsPath, MAX_PATH);
	PathRemoveFileSpec(settingsPath);
	strcat_s(settingsPath, MAX_PATH, "\\");
	strcat_s(settingsPath, MAX_PATH, SETTINGS_PATH);

	if (GetPrivateProfileStringA("SOCKET", "Host", "", buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			host = strResult.c_str();
		}
		catch (...){}
	}

	if (GetPrivateProfileStringA("SOCKET", "Port", 0, buffer, sizeof(buffer), settingsPath))
	{
		strResult = string(buffer);
		try
		{
			port = atoi(strResult.c_str());
		}
		catch (...){}
	}

}