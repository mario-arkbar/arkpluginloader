/*

Socket Class by Xstasy (Daniel Gothenborg) <daniel@dgw.no>
Allows interaction with server through simple sockets.

*/

#include "CSocket.h"
#include "lib\socket.io\sio_client.h"
#include <functional>
#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <string>
#include <stdio.h>
#include <tchar.h>

static CConsoleExtension console;
using namespace sio;
using namespace std;
std::mutex _lock;

std::condition_variable_any _cond;
bool connect_finish = false;

static socket::ptr current_socket;

#define HIGHLIGHT(__O__) std::cout<<"\e[1;31m"<<__O__<<"\e[0m"<<std::endl
#define EM(__O__) std::cout<<"\e[1;30;1m"<<__O__<<"\e[0m"<<std::endl

static string socketHost;
static int socketPort;

class connection_listener
{
	sio::client &handler;

public:

	connection_listener(sio::client& h) :
		handler(h)
	{
	}


	void on_connected()
	{
		_lock.lock();
		_cond.notify_all();
		connect_finish = true;
		char message[MAX_PATH];
		sprintf_s(message, "Socket connected to %s on port %d", socketHost, socketPort);
		console.Message(message);
		if (current_socket)
			CSocket::Init();
		_lock.unlock();
	
	}
	void on_close(client::close_reason const& reason)
	{
		_lock.lock();
		_cond.notify_all();
		char message[MAX_PATH];
		sprintf_s(message, "Lost connection: %s", reason);
		console.Message(message);
		_lock.unlock();
	}

	void on_fail()
	{
		_lock.lock();
		_cond.notify_all();
		console.Message("Lost connection to ARK.Bar");
		_lock.unlock();
	}
};

int participants = -1;



void bind_events(socket::ptr &socket)
{
	current_socket->on("load", sio::socket::event_listener_aux([&](string const& name, message::ptr const& data, bool isAck, message::ptr &ack_resp)
	{
		_lock.lock();
		string cname = data->get_map()["name"]->get_string();
		CPluginLoader::LoadPlugin(cname);
		_lock.unlock();
	}));

	current_socket->on("unload", sio::socket::event_listener_aux([&](string const& name, message::ptr const& data, bool isAck, message::ptr &ack_resp)
	{
		_lock.lock();
		string cname = data->get_map()["name"]->get_string();
		CPluginLoader::UnloadPlugin(cname);
		_lock.unlock();
	}));

	current_socket->on("reload", sio::socket::event_listener_aux([&](string const& name, message::ptr const& data, bool isAck, message::ptr &ack_resp)
	{
		_lock.lock();
		string cname = data->get_map()["name"]->get_string();
		CPluginLoader::ReloadPlugin(cname);
		_lock.unlock();
	}));

	current_socket->on("listplugins", sio::socket::event_listener_aux([&](string const& name, message::ptr const& data, bool isAck, message::ptr &ack_resp)
	{
		_lock.lock();
		string json = "{ \"plugins\": [";
		for (it_type iterator = CPluginLoader::pluginMap.begin(); iterator != CPluginLoader::pluginMap.end(); iterator++) {
			json += "{\"name\": \"" + iterator->first + "\"}, ";
		}
		json = json.substr(0, json.length() - 2) + "] }";
		current_socket->emit("pluginlist", json);
		_lock.unlock();
	}));

}

void CSocket::Connect(string host, int port)
{

	socketHost = host;
	socketPort = port;

	sio::client h;
	connection_listener l(h);

	h.set_open_listener(std::bind(&connection_listener::on_connected, &l));
	h.set_close_listener(std::bind(&connection_listener::on_close, &l, std::placeholders::_1));
	h.set_fail_listener(std::bind(&connection_listener::on_fail, &l));
	h.connect("http://" + host + ":" + std::to_string(port));
	_lock.lock();


	if (!connect_finish)
	{
		_cond.wait(_lock);
	}
	_lock.unlock();
	current_socket = h.socket();

	CSocket::Init();

	bind_events(current_socket);

	while (true)
	{
		Sleep(10);
	}

}

void CSocket::Init()
{
	current_socket->emit("hello");
}

void CSocket::Send(string command, string data)
{
	if (current_socket)
	{
		current_socket->emit(command, data);
	}
		
}
