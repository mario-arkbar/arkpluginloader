
#include "ConsoleExtension\ConsoleExtension.h"
#include "API.h"

static CConsoleExtension console;

class ARKAPIImpl : public API
{

	void ConsoleMessage(string p, string s);
	void ConsoleMessage(string p, string s, Console::Color c);
	void Release();
};

void ARKAPIImpl::Release()
{
	delete this;
}

void ARKAPIImpl::ConsoleMessage(string p, string s)
{
	console.Message(p, s);
}

void ARKAPIImpl::ConsoleMessage(string p, string s, Console::Color c)
{
	console.Message(p, s);
}

void API::ConsoleMessage(string p, string s) {
	console.Message(p, s);
}

void API::ConsoleMessage(string p, string s, Console::Color c) {
	console.Message(p, s, (CConsoleExtension::Color)c);
}

ARKAPI APIHANDLE APIENTRY GetAPI()
{
	return new ARKAPIImpl;
}
